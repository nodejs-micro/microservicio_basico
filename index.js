// import.
import {Adaptador} from "./Adapters";

// Adapter.
const resp = Adaptador({
  color: "amarillo", // Color.

  info: {
    Nombre: "Michael",
    Apellidp: "Quispe_Solorzano",
    Edad: "24",
    Nacionalidad: "EEUU",
    Editor_de_Texto: "Atom",
    Genero: "Masculino",
  },
});
console.log(resp);
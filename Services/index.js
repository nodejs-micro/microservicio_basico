//Import .
import { PersonalData, PreferencesColor } from "../Controllers";

//funcion servicio.

export function Servicio({ info, color }) {

  const personalData = PersonalData({ info }); // controler personalData.
  const preferencesColor = PreferencesColor({ color }); // controler preferencesColor.
  return { personalData, preferencesColor }; // return personalData.
}


